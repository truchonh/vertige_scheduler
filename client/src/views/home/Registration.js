import {Fab, Grid, TextField, Typography} from '@material-ui/core'
import {Check, Send} from '@material-ui/icons'
import React, {useState} from 'react'
import {useHistory} from 'react-router-dom'
import {makeStyles} from '@material-ui/core/styles'
import {MuiPickersUtilsProvider, TimePicker} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/dayjs'
import Container from '@material-ui/core/Container'
import dayjs from 'dayjs'
import Box from '@material-ui/core/Box'
import clsx from 'clsx'
import CircularProgress from '@material-ui/core/CircularProgress'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'
import green from '@material-ui/core/colors/green'
import * as superagent from 'superagent'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    marginTop: {
        '& > *': {
            marginTop: theme.spacing(3),
        }
    },
    extendedIcon: {
        marginLeft: theme.spacing(1),
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    buttonSuccess: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -15,
        marginLeft: -15,
    },
    title: {
        flexGrow: 1,
        color: theme.palette.text.primary,
    },
}));

export default function Registration() {
    const defaultFormState = {
        email: '',
        time_start: dayjs().startOf('day').hour(16),
        time_end: dayjs().startOf('day').hour(18),
    }

    const classes = useStyles()
    const history = useHistory()

    if (window.localStorage.getItem('is_booking_home')) {
        const value = JSON.parse(window.localStorage.getItem('is_booking_home'))
        value.value && history.push('/reserver')
    }

    const [formData, setFormData] = useState(defaultFormState)
    const [snackbar, setSnackbar] = useState({
        open: false,
        level: 'error',
        message: ''
    })
    const [loading, setLoading] = React.useState(false)
    const [success, setSuccess] = React.useState(false)

    const buttonClassname = clsx({
        [classes.buttonSuccess]: success,
    })

    const handleFormChange = e => {
        const { name, value } = e.target
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }
        setSnackbar(prevState => ({
            ...prevState,
            open: false
        }))
    }

    const handleFormSubmit = () => {
        superagent.post('/api/registration').send({
            email: formData.email,
            preferredStart: formData.time_start.format('HH:mm'),
            preferredEnd: formData.time_end.format('HH:mm'),
            dailyScheduleAlert: formData.time_start.subtract(1, 'hour').format('HH:mm'),
            alertThreshold: 4,
        })
            .then(() => {
                setLoading(false)
                setSuccess(true)
                setFormData(defaultFormState)
                setSnackbar({
                    open: true,
                    level: 'success',
                    message: `Demande bien reçu ! Une courriel de confirmation t'a été envoyé.`
                })
            })
            .catch(err => {
                setLoading(false)
                setSuccess(false)
                if (err.status && err.status === 400) {
                    setSnackbar({
                        open: true,
                        level: 'error',
                        message: err.response.text
                    })
                }
            })
    }

    return (
        <Container maxWidth="sm" fixed>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Réservation Vertige
                    </Typography>
                    <Button color="inherit" onClick={() => history.push('/reserver')}>Réserver</Button>
                </Toolbar>
            </AppBar>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography variant="h4" align="center">Assistant de réservation du Vertige</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1" align="center">
                        Tané de ne pas pouvoir grimper à ton heure préférée parce que tu as oublié de réserver ?
                        <i> J'ai l'outil qu'il te faut !</i>
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1" align="center">
                        En nous fournissant quelques informations, on va non seulement t'envoyer un rappel s'il ne reste bientôt
                        plus de place au Vertige, mais on va aussi te simplifier la vie en réservant à ta place !
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <form noValidate autoComplete="off">
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Box pt={1}>
                                    <Typography variant="body2">
                                        Indique ta plage horaire de grimpe favorite :
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item xs={6}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <TimePicker
                                        variant="inline"
                                        autoOk
                                        inputVariant="outlined"
                                        minutesStep={30}
                                        value={formData.time_start}
                                        onChange={value => handleFormChange({ target: {name: 'time_start', value} })}
                                        label="Début"
                                        ampm={false}
                                        fullWidth
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={6}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <TimePicker
                                        variant="inline"
                                        autoOk
                                        inputVariant="outlined"
                                        minutesStep={30}
                                        value={formData.time_end}
                                        onChange={value => handleFormChange({ name: 'time_end', value })}
                                        label="Fin"
                                        ampm={false}
                                        fullWidth
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField name="email" label="Courriel" variant="outlined" fullWidth
                                           value={formData.email} onChange={handleFormChange} required={true}/>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1" align="center">
                        Une fois ton inscription reçue, un rappel te sera envoyé une heure avant ta plage horaire de grimpe (ou dès qu'il ne
                        reste plus beaucoup de places). À partir de cette notification, tu pourras envoyer ta réservation <i>en un clic !</i>
                    </Typography>
                </Grid>
                <Grid item xs={12} align="center">
                    <div className={classes.wrapper}>
                        <Fab color="primary" variant="extended"
                             onClick={handleFormSubmit}
                             disabled={loading}
                             className={buttonClassname}>
                            S'inscrire
                            {success ? <Check className={classes.extendedIcon}/> : <Send className={classes.extendedIcon}/>}
                        </Fab>
                        {loading && <CircularProgress size={30} className={classes.buttonProgress} />}
                    </div>
                </Grid>
            </Grid>
            <Snackbar open={snackbar.open} onClose={handleClose}>
                <Alert onClose={handleClose} severity={snackbar.level}>
                    {snackbar.message}
                </Alert>
            </Snackbar>
        </Container>
    )
}