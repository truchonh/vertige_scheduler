import {Container, Fab, Grid} from '@material-ui/core'
import React, {useEffect, useState} from 'react'
import Typography from '@material-ui/core/Typography'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import {useLocation} from "react-router-dom"
import * as superagent from 'superagent'
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar'
import {DatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers'
import dayjs from 'dayjs'
import {makeStyles} from '@material-ui/core/styles'
import ScheduleSelectionList from './ScheduleSelectionList'
import ContactInfoForm from './ContactInfoForm'
import DateFnsUtils from '@date-io/dayjs'
import CircularProgress from '@material-ui/core/CircularProgress'
import green from '@material-ui/core/colors/green'
import clsx from 'clsx'
import {Check, InsertInvitation} from '@material-ui/icons'
import Box from '@material-ui/core/Box'
import Hidden from '@material-ui/core/Hidden'
import {FacebookProvider, Page} from 'react-facebook'
import Button from '@material-ui/core/Button'

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',

    },
    formTitle: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > *:not(:first-child)': {
            marginLeft: theme.spacing(1)
        }
    },
    datePicker: {
        fontSize: theme.typography.h5.fontSize,
        width: '122px'
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    buttonSuccess: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -15,
        marginLeft: -15,
    },
    extendedIcon: {
        marginLeft: theme.spacing(1),
    },
    homeConfigCheckbox: {
        position: 'fixed',
        bottom: 0,
        right: 0,
        zoom: '60%'
    }
}));

export default function Booking() {
    const classes = useStyles()
    const query = useQuery()

    const [formData, setFormData] = useState({})
    const [saveLocally, setSaveLocally] = useState(false)
    const [snackbar, setSnackbar] = useState({
        open: false,
        level: 'error',
        message: ''
    })
    const [selectedSchedule, setSelectedSchedule] = useState(
        query.has('debut') && query.has('fin') ? (query.get('debut') + ' - ' + query.get('fin')) : ''
    )
    const [selectedDate, setSelectedDate] = useState(dayjs().startOf('day'))
    const [loading, setLoading] = React.useState(false)
    const [success, setSuccess] = React.useState(false)
    const [isHomePage, setHomePage] = useState(false)

    const buttonClassname = clsx({
        [classes.buttonSuccess]: success,
    })

    useEffect(() => {
        if (window.localStorage.getItem('booking_form')) {
            setSaveLocally(true)
        }
        if (window.localStorage.getItem('is_booking_home')) {
            const value = JSON.parse(window.localStorage.getItem('is_booking_home'))
            setHomePage(value.value)
        }
    }, [])

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }
        setSnackbar(prevState => ({
            ...prevState,
            open: false
        }))
    }

    const handleFormSubmit = async () => {
        setLoading(false)
        setSuccess(false)
        const lastBookingDate = window.localStorage.getItem('last_booking')
        if (lastBookingDate && dayjs(JSON.parse(lastBookingDate).date).isSame(dayjs(), 'day')) {
            setSnackbar({
                open: true,
                level: 'warning',
                message: `Une réservation a déjà été envoyé aujourd'hui. Envoyez à nouveau pour confirmer.`
            })
            window.localStorage.removeItem('last_booking')
            return
        }

        setLoading(true)
        superagent.post('/api/booking').send({
            ...formData,
            bookingDate: selectedDate,
            timeStart: selectedSchedule && selectedSchedule.split(' - ')[0],
            timeEnd: selectedSchedule && selectedSchedule.split(' - ')[1],
        })
            .then(() => handleBookingSuccess())
            .catch(err => handleBookingError(err))
    }

    const handleBookingError = err => {
        setLoading(false)
        setSuccess(false)
        if (err.status && err.status === 400) {
            setSnackbar({
                open: true,
                level: 'error',
                message: err.response.text
            })
        }
    }

    const handleBookingSuccess = () => {
        if (saveLocally) {
            window.localStorage.setItem('booking_form', JSON.stringify(formData))
        } else {
            window.localStorage.removeItem('booking_form')
        }

        window.localStorage.setItem('last_booking', JSON.stringify({ date: new Date() }))

        setSelectedSchedule('')
        setLoading(false)
        setSuccess(true)
        setSnackbar({
            open: true,
            level: 'success',
            message: 'Réservation reçu avec succès, la confirmation vous sera envoyé sous peu par courriel.'
        })
    }

    const handleScheduleChange = (e) => {
        setSelectedSchedule(e.target.value)
    }

    const handleSaveLocallyChange = e => {
        setSaveLocally(e.target.checked)
    }

    const handleSetHomePage = e => {
        setHomePage(e.target.checked)
        window.localStorage.setItem('is_booking_home', JSON.stringify({ value: e.target.checked }))
    }

    return (
        <Box className={classes.root} position="relative">
            <Container maxWidth="sm" fixed>
                <Grid container spacing={2}>
                    <Grid item xs={12} align="center" className={classes.formTitle}>
                        <Typography variant="h5" display="inline">
                            Réservation pour&nbsp;
                        </Typography>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                                InputProps={{
                                    classes: {
                                        root: classes.datePicker
                                    }
                                }}
                                variant="inline" size="small" autoOk
                                value={selectedDate} onChange={setSelectedDate}
                                disablePast={true}
                                disableToolbar={true}
                                maxDate={dayjs().startOf('day').add(6, 'day')}
                                labelFunc={(date) => {
                                    if (date.isSame(dayjs(), 'date')) {
                                        return `aujourd'hui`;
                                    } else {
                                        switch (date.day()) {
                                            case 0: return 'dimanche'
                                            case 1: return 'lundi'
                                            case 2: return 'mardi'
                                            case 3: return 'mercredi'
                                            case 4: return 'jeudi'
                                            case 5: return 'vendredi'
                                            case 6: return 'samedi'
                                            default: return date.format('YYYY-MM-DD')
                                        }
                                    }
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12} align="center">
                        <ScheduleSelectionList onChange={handleScheduleChange} selectedValue={selectedSchedule} date={selectedDate}/>
                    </Grid>
                    <Grid item xs={12}>
                        <ContactInfoForm formData={formData} onChange={setFormData}/>
                    </Grid>
                    <Grid item xs={12} align="right">
                        <FormControlLabel
                            control={
                                <Checkbox checked={saveLocally} onChange={handleSaveLocallyChange}
                                          name="save_locally" color="primary"/>
                            }
                            label="Sauvegarder ces informations"
                        />
                    </Grid>
                    <Grid item xs={12} align="center">
                        <div className={classes.wrapper}>
                            <Fab
                                color="primary"
                                variant="extended"
                                onClick={handleFormSubmit}
                                disabled={loading}
                                className={buttonClassname}
                            >
                                Confirmer la réservation
                                {success ? <Check className={classes.extendedIcon}/> : <InsertInvitation className={classes.extendedIcon}/>}
                            </Fab>
                            {loading && <CircularProgress size={30} className={classes.buttonProgress} />}
                        </div>
                    </Grid>
                </Grid>
                <Snackbar
                    open={snackbar.open}
                    onClose={handleClose}
                >
                    <Alert onClose={handleClose} severity={snackbar.level}>
                        {snackbar.message}
                    </Alert>
                </Snackbar>
            </Container>
            <Hidden smDown>
                <FacebookProvider appId="1305808589760833">
                    <Page href="https://www.facebook.com/vertigeescalade/" tabs="timeline" colorScheme="dark"
                          width={300} height={625} smallHeader={true} hideCover={true}/>
                </FacebookProvider>
            </Hidden>
            <FormControlLabel
                className={classes.homeConfigCheckbox}
                control={
                    <Checkbox checked={isHomePage}
                              onChange={handleSetHomePage}
                              name="save_locally"
                              color="default"
                    />
                }
                label="Page d'acceuil"
            />
        </Box>
    );
}
