import React, {useEffect, useState} from 'react'
import {Grid, TextField} from '@material-ui/core'
import GoogleMapsPlaceInput from './GoogleMapsPlaceInput'

export default function ContactInfoForm(props) {
    const {onChange} = props

    // let autocomplete = null;
    const [form, setValues] = useState({
        name: '',
        family_name: '',
        email: '',
        phone: '',
        street_address: '',
        city: '',
        province: '',
        country: '',
        postal_code: '',
    })

    useEffect(() => {
        const formDataJson = window.localStorage.getItem('booking_form')
        if (formDataJson) {
            const formData = JSON.parse(formDataJson)
            setValues(formData)
            onChange(formData)
        }
    }, [])

    const handlePlaceSelect = (newValue) => {
        let address = newValue.address_components
        setValues(prevState => {
            const newState = {
                ...prevState,
                street_address: `${address.find(_part => _part.types.includes("street_number")).long_name} ${address.find(_part => _part.types.includes("route")).long_name}`,
                city: address.find(_part => _part.types.includes("locality")).long_name,
                province: address.find(_part => _part.types.includes("administrative_area_level_1")).long_name,
                country: address.find(_part => _part.types.includes("country")).long_name,
                postal_code: address.find(_part => _part.types.includes("postal_code")).long_name,
            }
            onChange(newState)
            return newState
        })
    }

    const handleFormChange = e => {
        const { name, value } = e.target
        setValues(prevState => {
            const newState = {
                ...prevState,
                [name]: value
            }
            onChange(newState)
            return newState
        })
    }

    return (
        <form noValidate autoComplete="off">
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <TextField name="name" label="Prénom" variant="outlined" fullWidth
                               value={form.name} onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={6}>
                    <TextField name="family_name" label="Nom" variant="outlined" fullWidth
                               value={form.family_name} onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={12}>
                    <TextField name="email" label="Courriel" variant="outlined" fullWidth
                               value={form.email} onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={12}>
                    <TextField name="phone" label="Téléphone" variant="outlined" fullWidth
                               value={form.phone} onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={12}>
                    <GoogleMapsPlaceInput onChange={handlePlaceSelect} />
                </Grid>
                <Grid item xs={12}>
                    <TextField name="street_address" label="Adresse" variant="outlined" fullWidth
                               value={form.street_address} onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={6}>
                    <TextField name="city" label="Ville" variant="outlined" fullWidth
                               value={form.city} onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={6}>
                    <TextField name="province" label="Province" variant="outlined" fullWidth
                               value={form.province}  onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={6}>
                    <TextField name="postal_code" label="Code postal" variant="outlined" fullWidth
                               value={form.postal_code}  onChange={handleFormChange} required={true}/>
                </Grid>
                <Grid item xs={6}>
                    <TextField name="country" label="Pays" variant="outlined" fullWidth
                               value={form.country}  onChange={handleFormChange} required={true}/>
                </Grid>
            </Grid>
        </form>
    );
}
