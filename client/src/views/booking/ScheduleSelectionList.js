import React, {useEffect, useState} from 'react'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import * as superagent from 'superagent'
import MenuItem from '@material-ui/core/MenuItem'
import {makeStyles} from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import dayjs from 'dayjs'

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 325,
    },
}));

export default function ScheduleSelectionDialog(props) {
    // scale the plot data to the svg container size
    const X_RATIO = 1000*60*60*24*7 / 100 // 2 days (in ms) 100 pixels wide
    const Y_RATIO = 45 / 34 // max capacity of 45, over 34 pixels high

    const classes = useStyles()
    const {onChange, selectedValue, date} = props
    const [schedules, setSchedules] = useState([])
    const [plotData, setPlotData] = useState(new Map())

    const fetchPlotData = async () => {
        const res = await superagent.get(`/api/schedule/capacity_plot/${date.day()}`)
        const plotMap = new Map()

        for (let _plotObj of res.body) {
            const dataPoints = _plotObj.plot.map(_point => [
                Math.round(Math.max(_point.distance, 0) / X_RATIO),
                (Math.round(_point.capacity / Y_RATIO * 10) / 10) + 1
            ])

            const sampledValues = [];
            let pointsSum = 0;
            let pointsCount = 0;
            dataPoints.forEach((value, index) => {
                pointsSum += value[1]
                pointsCount++

                if (
                    index % 3 === 0 ||
                    index === 0 ||
                    index === dataPoints.length -1
                ) {
                    sampledValues.push([
                        value[0],
                        Math.round(pointsSum / pointsCount * 10) / 10
                    ])
                    pointsSum = pointsCount = 0
                }
            });

            plotMap.set(_plotObj.period, sampledValues)
        }
        return plotMap;
    }

    const fetchSchedules = async () => {
        const res = await superagent.get(`/api/schedule?date=${date.format('YYYY-MM-DD')}`)
        return res.body
    }

    useEffect(() => {
        const _wrapper = async () => {
            try {
                const plotMap = await fetchPlotData()
                const schedules = await fetchSchedules()
                setPlotData(plotMap)
                setSchedules(schedules)
            } catch (err) {
                // display an error ?
                console.error(err)
            }
        }
        _wrapper()
    }, [date])

    return (
        <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="schedule-selection-label">Plage horaire</InputLabel>
            <Select
                labelId="schedule-selection-label"
                id="schedule-selection"
                value={selectedValue}
                onChange={onChange}
                label="Plage horaire"
            >
                <MenuItem value="">
                    <em>Aucune</em>
                </MenuItem>
                {schedules.map(_schedule => {
                    const plot = plotData.get(_schedule.startPeriod.rawTimeStart) || []
                    const coordinateStr = plot.map(_point => _point.join(',')).join(' ')

                    const distanceToNow = dayjs(_schedule.startPeriod.dateStart).valueOf() - dayjs().valueOf()

                    return (
                        <MenuItem
                            value={_schedule.startPeriod.rawTimeStart + ' - ' + _schedule.endPeriod.rawTimeEnd}
                            key={_schedule.startPeriod._id} style={{ paddingTop: 0, paddingBottom: 0, minHeight: '48px' }}
                            disabled={_schedule.capacity === 0}
                        >
                            <Box display="flex" style={{ width: '100%' }}>
                            <span style={{ flexGrow: 1, textAlign: 'left', alignSelf: 'center' }}>
                                ({_schedule.capacity} dispo) {_schedule.startPeriod.rawTimeStart} - {_schedule.endPeriod.rawTimeEnd}
                            </span>
                                {/*A bit of a copy of Github's repository activity plot*/}
                                <svg width="100" height="36" style={{ marginTop: '-8px', marginBottom: '-8px' }}>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="0.5,0 0.5,36">
                                    </polyline>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="14,0 14,36">
                                    </polyline>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="29,0 29,36">
                                    </polyline>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="43,0 43,36">
                                    </polyline>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="57,0 57,36">
                                    </polyline>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="71,0 71,36">
                                    </polyline>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="86,0 86,36">
                                    </polyline>
                                    <polyline stroke="#777"
                                              stroke-width="0.5"
                                              stroke-dasharray="3,3"
                                              points="99.5,0 99.5,36">
                                    </polyline>
                                    <defs>
                                        <linearGradient id={`gradient-${_schedule.startPeriod._id}`}
                                                        x1="0" x2="0" y1="1" y2="0">
                                            <stop offset="10%" stop-color="#ec407a"></stop>
                                            <stop offset="33%" stop-color="#f06292"></stop>
                                            <stop offset="66%" stop-color="#f48fb1"></stop>
                                            <stop offset="90%" stop-color="#f8bbd0"></stop>
                                        </linearGradient>
                                        <mask id={`sparkline-${_schedule.startPeriod._id}`} x="0" y="0"
                                              width="100" height="34">
                                            <polyline transform="translate(100, 34) scale(-1,-1)"
                                                      fill="transparent"
                                                      stroke="#f8bbd0"
                                                      stroke-width="2"
                                                      points={coordinateStr}>
                                            </polyline>
                                        </mask>
                                    </defs>
                                    <g transform="translate(0, 2.0)">
                                        <rect x="0" y="-2" width="100" height="36" style={{
                                            stoke: 'none',
                                            fill: `url(#gradient-${_schedule.startPeriod._id})`,
                                            mask: `url(#sparkline-${_schedule.startPeriod._id})`
                                        }}></rect>
                                    </g>
                                    <polyline stroke="#536dfe"
                                              stroke-width="1"
                                              points={`${100 - (distanceToNow / X_RATIO)},0 ${100 - (distanceToNow / X_RATIO)},36`}>
                                    </polyline>
                                </svg>
                            </Box>
                        </MenuItem>
                    )
                })}
            </Select>
        </FormControl>
    );
}
