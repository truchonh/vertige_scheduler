import React, {useEffect, useState} from 'react'
import {Container, Typography} from '@material-ui/core'
import {useLocation, useHistory} from 'react-router-dom'
import * as superagent from 'superagent'

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

export default function Unsubscribe() {
    const query = useQuery()
    const history = useHistory()

    const [unsubscribeSuccess, setUnsubscribeSuccess] = useState(false)
    useEffect(() => {
        if (!query.has('user')) {
            history.push('/')
        }

        superagent.delete(`/api/preference/${query.get('user')}`)
            .then(res => {
                setUnsubscribeSuccess(true)
            })
            .catch(err => {
                history.push('/')
            })
    }, [])

    return (
        <Container>
            <Typography variant="h4" align="center" hidden={!unsubscribeSuccess}>
                Vôtre courriel a été retiré de la liste de notification.
            </Typography>
        </Container>
    );
}