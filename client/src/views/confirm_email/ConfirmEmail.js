import React, {useEffect, useState} from 'react'
import {useLocation} from 'react-router-dom'
import {Container, Typography} from '@material-ui/core'
import * as superagent from 'superagent'

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

export default function ConfirmEmail() {
    const query = useQuery()

    const [confirmSuccess, setConfirmSuccess] = useState(false)
    useEffect(() => {
        superagent.put(`/api/registration/${query.get('uuid')}/confirm`)
            .then(res => {
                setConfirmSuccess(true)
            })
            .catch(err => {

            })
    }, [])

    return (
        <Container>
            <Typography variant="h4" align="center" hidden={!confirmSuccess}>
                Ton courriel a été vérifié avec succès !
            </Typography>
            <Typography variant="h6" align="center" hidden={!confirmSuccess}>
                Des notifications vous seront maintenant envoyé quotidiennement, selon vos préférences.
            </Typography>
        </Container>
    );
}