import React from 'react'
import {createMuiTheme, makeStyles, ThemeProvider} from '@material-ui/core/styles'
import Registration from './views/home/Registration'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Booking from './views/booking/Booking'
import Unsubscribe from './views/unsubscribe/Unsubscribe'
import ConfirmEmail from './views/confirm_email/ConfirmEmail'
import Box from '@material-ui/core/Box'
import CssBaseline from '@material-ui/core/CssBaseline'
import { frFR } from '@material-ui/core/locale';

const darkTheme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#7986cb'
        }
    },
}, frFR)

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: '#282c34',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '100vh',
        color: darkTheme.palette.text.primary
    },
}))

export default function App() {
    const classes = useStyles()
    return (
        <Router>
            <CssBaseline />
            <ThemeProvider theme={darkTheme}>
                <Box className={classes.root} py={2}>
                    <Switch>
                        <Route path="/confirm_email">
                            <ConfirmEmail/>
                        </Route>
                        <Route path="/unsubscribe">
                            <Unsubscribe/>
                        </Route>
                        <Route path="/reserver">
                            <Booking/>
                        </Route>
                        <Route path="/">
                            <Registration/>
                        </Route>
                    </Switch>
                </Box>
            </ThemeProvider>
        </Router>
    )
}
