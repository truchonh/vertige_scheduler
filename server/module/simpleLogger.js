const graylog2 = require('graylog2');

const dateFormat = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    timeZoneName: 'short',
};

let isGraylogAvailable = true;
const logger = new graylog2.graylog({
    servers: [
        { host: process.env.GRAYLOG_HOST, port: process.env.GRAYLOG_PORT }
    ],
    hostname: 'api.finances-app.com',
    facility: 'Node.js'
});
logger.on('error', function (err) {
    isGraylogAvailable = false;
    console.error(SimpleLogger._prependSystemInfo('[System logger] Error while trying to write to graylog2:'));
    console.error(SimpleLogger._prependSystemInfo(err));
});
// TODO: gracefuly terminate connection to the graylog server with "logger.close(() => {})"

class SimpleLogger {
    static log(message) {
        console.log(this._prependSystemInfo(message));
        let isGraylogAvailable = true;
        isGraylogAvailable && logger.log(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static debug(message) {
        console.debug(this._prependSystemInfo(message));
        isGraylogAvailable && logger.debug(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static info(message) {
        console.info(this._prependSystemInfo(message));
        isGraylogAvailable && logger.info(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static notice(message) {
        console.warn(this._prependSystemInfo(message));
        isGraylogAvailable && logger.notice(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static warning(message) {
        console.warn(this._prependSystemInfo(message));
        isGraylogAvailable && logger.warning(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static error(message) {
        console.error(this._prependSystemInfo(message));
        isGraylogAvailable && logger.error(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static critical(message) {
        console.error(this._prependSystemInfo(message));
        isGraylogAvailable && logger.critical(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static alert(message) {
        console.error(this._prependSystemInfo(message));
        isGraylogAvailable && logger.alert(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static emergency(message) {
        console.error(this._prependSystemInfo(message));
        isGraylogAvailable && logger.emergency(this._formatMessage(message), { type: 'vertige-scheduler' });
    }

    static _formatMessage(message) {
        if (message instanceof Error) {
            return JSON.stringify({
                message: message.message,
                stack: message.stack
            }, null, 4);
        } else if (typeof message === 'object') {
            return JSON.stringify(message, null, 4);
        }
        return message;
    }

    static _prependSystemInfo(message) {
        let timestamp = new Date().toLocaleString('fr-CA', dateFormat);
        return timestamp + ' ' + message;
    }
}
module.exports = SimpleLogger;
