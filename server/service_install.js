const Service = require('node-windows').Service;
const path = require('path');

// Create a new service object
const svc = new Service({
    name: 'Vertige Scheduler API',
    description: 'All-in-one server for the Vertige Scheduler web app',
    script: path.join(__dirname, 'server.js'),
});

svc.on('uninstall', function() {
    svc.install();
});
svc.on('alreadyuninstalled', function() {
    svc.install();
});
svc.on('install',function(){
    svc.start();
});

svc.uninstall();