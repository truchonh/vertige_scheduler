const express = require('express');
const mongoose = require('mongoose');

const logger = require('../module/simpleLogger');
const mailer = require('./mailer');

const Schedule = mongoose.model('schedule', require('../collection/schedule'));
const CapacityPlotDataCache = mongoose.model('capacityPlotDataCache', require('../collection/capacityPlotDataCache'));

class ScheduleController {
    static getRouter() {
        const router = express.Router();

        router.route('/').get((req, res) => this._handleGetSchedule(req, res));
        router.route('/capacity_plot/:week_day').get((req, res) => this._handleGetCapacityPlot(req, res));

        return router;
    }

    static async _handleGetSchedule(req, res) {
        const date = req.query.date;

        try {
            const latestSchedules = await Schedule.find({
                source: 'vertige'
            })
                .limit(1)
                .sort('-creationDate');
            const availableSchedules = latestSchedules[0].findAvailableSlotsByDay(date);
            res.send(availableSchedules);
        } catch (err) {
            logger.error(err);
            res.status(500).end();
            await mailer.sendAdminNotification(err);
        }
    }

    static async _handleGetCapacityPlot(req, res) {
        const dayOfWeek = req.params.week_day;

        try {
            const cacheEntry = await CapacityPlotDataCache.findOne({ dayOfWeek: parseInt(dayOfWeek) });
            res.send(cacheEntry ? cacheEntry.data : []);
        } catch (err) {
            logger.error(err);
            res.status(500).end();
            await mailer.sendAdminNotification(err);
        }
    }
}

module.exports = ScheduleController;
