const nodemailer = require('nodemailer');
const fs = require('fs');
const util = require('util');
const path = require('path');
const mjml = require('mjml');
const _ = require('lodash');
const dayjs = require('dayjs');

const fs_readFile = util.promisify(fs.readFile);

const logger = require('../module/simpleLogger');
const config = require('../mailer_config.json');

let transporter = null;

class mailService {
    /**
     * Send an email with custom content.
     * @param {object} options
     * @param {string} options.from
     * @param {string} options.to
     * @param {string} options.subject
     * @param {string} options.text
     * @param {string} options.html
     * @returns {Promise<any>}
     */
    static async sendEmail(options) {
        let transporter = getTransporter();
        return new Promise((resolve, reject) => {
            transporter.sendMail(options, (error, info) => {
                if (error) {
                    return reject(error);
                }
                return resolve(info);
            });
        });
    }

    /**
     * @param {object} options
     * @param {number} options.capacityLeft
     * @param {BookingSlot[]} options.availableSlots
     * @param {Preference} options.preferences
     * @returns {Promise<any>}
     */
    static async notifyPreferredSlotLowCapacity(options) {
        const preferredSlot = options.availableSlots
            .find(_slot => _slot.startPeriod.rawTimeStart === options.preferences.preferredStart);

        const res = await this._buildEmailNotification('daily_schedule.html', {
            server_host: process.env.PUBLIC_HOST,
            isLowCapacityAlert: true,
            timeSlots: options.availableSlots.map(_slot => ({
                timeStart: _slot.startPeriod.rawTimeStart,
                timeEnd: _slot.endPeriod.rawTimeEnd,
                capacity: _slot.capacity
            })),
            update_time: dayjs().format('H:mm'),
            preferredSlot: {
                timeStart: preferredSlot.startPeriod.rawTimeStart,
                timeEnd: preferredSlot.endPeriod.rawTimeEnd,
                capacity: preferredSlot.capacity
            },
            preference_id: options.preferences._id
        });

        await this.sendEmail({
            ...options,
            from: config.sender_email,
            to: options.preferences.email,
            subject: `(Réservations Vertige) ${options.capacityLeft} places disponibles pour ${options.preferences.preferredStart}`,
            text: '',
            html: res.html,
        });
        logger.debug(`Low capacity notification sent to ${options.preferences._id}.`);
    }

    /**
     * @param {object} options
     * @param {BookingSlot[]} options.availableSlots
     * @param {Preference} options.preferences
     * @returns {Promise<any>}
     */
    static async sendDailyAvailableSlots(options) {
        const res = await this._buildEmailNotification('daily_schedule.html', {
            server_host: process.env.PUBLIC_HOST,
            isLowCapacityAlert: false,
            timeSlots: options.availableSlots.map(_slot => ({
                timeStart: _slot.startPeriod.rawTimeStart,
                timeEnd: _slot.endPeriod.rawTimeEnd,
                capacity: _slot.capacity
            })),
            update_time: dayjs().format('H:mm'),
            preferredSlot: null,
            preference_id: options.preferences._id,
        });

        await this.sendEmail({
            ...options,
            from: config.sender_email,
            to: options.preferences.email,
            subject: '(Réservations Vertige) Plages horaires pour la journée',
            text: '',
            html: res.html,
        });
        logger.debug(`Daily notification sent to ${options.preferences._id}.`);
    }

    /**
     * @param {object} options
     * @param {string} options.email
     * @param {string} options.uuid
     * @returns {Promise<void>}
     */
    static async sendRegistrationConfirmation(options) {
        let templatePath = path.join(__dirname, 'templates', 'email_confirmation.html');
        let template = await fs_readFile(templatePath, { encoding: 'utf-8' });

        const tplFn = _.template(template);
        let res = mjml(tplFn({
            server_host: process.env.PUBLIC_HOST,
            registration_uuid: options.uuid
        }), { minify: true });

        await this.sendEmail({
            from: config.sender_email,
            to: options.email,
            subject: 'Confirmation du email',
            html: res.html,
            text: ''
        });
        logger.debug(`Email confirmation sent for registration ${options.uuid}`);
    }

    /**
     * @param {object} options
     * @param {string} options.email
     * @param {string} options.bookingStart
     * @param {string} options.timeStart
     * @param {string} options.timeEnd
     * @param {string} options.preferenceId
     * @returns {Promise<void>}
     */
    static async sendBookingErrorNotification(options) {
        const res = await this._buildEmailNotification('booking_error.html', {
            server_host: process.env.PUBLIC_HOST,
            booking_date: options.bookingStart,
            time_start: options.timeStart,
            time_end: options.timeEnd,
            preference_id: options.preferenceId,
        });

        await this.sendEmail({
            ...options,
            from: config.sender_email,
            to: options.email,
            subject: '(Réservations Vertige) Erreur lors de la réservation',
            text: '',
            html: res.html,
        });
        logger.debug(`Booking error notification sent to ${options.preferenceId}.`);
    }

    static async sendAdminNotification(error) {
        const res = await this._buildEmailNotification('admin_notification.html', {
            timestamp: new Date().toLocaleString('fr-CA'),
            error_message: error.message,
            stack_trace: error.stack.split('\n').join('<br>')
        });

        await this.sendEmail({
            from: config.sender_email,
            to: 'hugo.truchonti@gmail.com',
            subject: 'Vertige scheduler system error',
            text: '',
            html: res.html,
        });
    }

    static async _buildEmailNotification(templateName, data) {
        const contentFilePath = path.join(__dirname, 'templates', templateName);
        const mainContentTemplate = await fs_readFile(contentFilePath, { encoding: 'utf-8' });
        const mainContent = _.template(mainContentTemplate)(data);

        const templateFilePath = path.join(__dirname, 'templates', 'notification_template.html');
        const notificationTemplate = await fs_readFile(templateFilePath, { encoding: 'utf-8' });

        const tplFn = _.template(notificationTemplate);
        return mjml(tplFn({
            server_host: process.env.PUBLIC_HOST,
            preference_id: data.preference_id,
            main_content: mainContent
        }), { minify: true });
    }
}
module.exports = mailService;

function getTransporter() {
    if (transporter === null) {
        transporter = nodemailer.createTransport(config);
    }
    return transporter;
}
