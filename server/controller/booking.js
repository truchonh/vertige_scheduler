const express = require('express');
const dayjs = require('dayjs');
const getIp = require('ipware')().get_ip;
const mongoose = require('mongoose');

const crawler = require('./crawler');
const mailer = require('./mailer');
const logger = require('../module/simpleLogger');

const Preference = mongoose.model('preference', require('../collection/preference'));

const bookingIpMap = new Map();

class BookingController {
    static getRouter() {
        const router = express.Router();

        router.route('/').post((req, res) => this._handleBooking(req, res));

        return router;
    }

    static async _handleBooking(req, res) {
        const ip = getIp(req).clientIp;
        const formData = req.body;

        let userPreference;
        try {
            const validationError = this._validateBookingData(formData);
            if (validationError) {
                return res.status(400).send(validationError);
            }

            userPreference = await Preference.findOne({ email: formData.email });
            if (!userPreference) {
                return res.status(403).end();
            }

            // if (
            //     bookingIpMap.has(ip) && bookingIpMap.get(ip).isSame(dayjs(), 'date') ||
            //     userPreference.lastBookingTimestamp && dayjs(userPreference.lastBookingTimestamp).isSame(dayjs(), 'date')
            // ) {
            //     return res.status(400).send('Seulement une demande de réservation est autorisé par jour.');
            // }

            const isPeriodAvailable = await crawler.checkScheduleAvailability(formData.bookingDate, formData.timeStart);
            if (!isPeriodAvailable) {
                return res.status(400).send(`La plage horaire choisi n'est plus disponible. Veuillez en choisir une autre.`);
            }

            bookingIpMap.set(ip, dayjs());
            res.status(202).end();

        } catch (err) {
            logger.error(err);
            await mailer.sendAdminNotification(err);
            return res.status(500).end();
        }

        try {
            await crawler.submitBooking(formData);
            logger.debug('Booking crawl script success.');
        } catch (err) {
            bookingIpMap.delete(ip);
            await mailer.sendBookingErrorNotification({
                email: userPreference.email,
                bookingStart: dayjs(formData.bookingDate).format('YYYY-MM-DD'),
                timeStart: formData.timeStart,
                timeEnd: formData.timeEnd,
                preferenceId: userPreference._id,
            });

            logger.error('Booking crawl script failed.');
            logger.error(err);
            await mailer.sendAdminNotification(err);
        }
    }

    static _validateBookingData(data) {
        const requiredFields = [
            'name', 'family_name', 'email', 'phone', 'street_address', 'city',
            'province', 'country', 'postal_code', 'bookingDate', 'timeStart', 'timeEnd'
        ];
        const dataKeys = Object.keys(data);
        if (requiredFields.some(_field => !dataKeys.includes(_field) || !data[_field])) {
            return 'Tous les champs du formulaire sont obligatoire.'
        } else if ((/[^@]+@[^\.]+\..+/gi).test(data.email) === false) {
            return 'Courriel non valide.';
        } else if ((/[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVXY]\ ?\d[ABCEGHJKLMNPRSTVXY]\d/gi).test(data.postal_code) === false) {
            return 'Code postal non valide.';
        }

        const timeStart = dayjs(data.bookingDate)
            .hour(data.timeStart.split(':')[0])
            .minute(data.timeStart.split(':')[1])
            .second(0)
            .millisecond(0);
        if (timeStart.isBefore(dayjs(data.bookingDate))) {
            return `Veuillez choisir une plage horaire qui n'est pas encore commencé.`;
        }
    }
}

module.exports = BookingController;
