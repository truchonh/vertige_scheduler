const express = require('express');
const { v4: uuid } = require('uuid');
const mongoose = require('mongoose');

const mailer = require('./mailer');
const logger = require('../module/simpleLogger');

const Registration = mongoose.model('registration', require('../collection/registration'));
const Preference = mongoose.model('preference', require('../collection/preference'));

class RegistrationController {
    static getRouter() {
        const router = express.Router();

        router.route('/:registration_uuid/confirm').put((req, res) => this._handleRegistrationConfirmation(req, res));
        router.route('/').post((req, res) => this._handleRegistration(req, res));

        return router;
    }

    static async _handleRegistrationConfirmation(req, res) {
        const uuid = req.params.registration_uuid;

        try {
            const registration = await Registration.findOne({ uuid });

            // TODO: Is that really the behaviour we want ?
            if (!registration) {
                return res.status(404).end();
            }

            const preference = new Preference(registration.data);
            await preference.save();
            await registration.delete();

            res.status(204).end();
        } catch (err) {
            logger.error(err);
            res.status(500).end();
            await mailer.sendAdminNotification(err);
        }
    }

    static async _handleRegistration(req, res) {
        const preferences = req.body;

        const validationError = this._validatePreferences(preferences);
        if (validationError) {
            // TODO: Maybe send more detail
            return res.status(400).send(validationError);
        }

        try {
            await Registration.deleteMany({ email: preferences.email });
            await Preference.deleteMany({ email: preferences.email });

            const registration = new Registration({
                email: preferences.email,
                uuid: uuid(),
                data: preferences
            });
            await registration.save();

            await mailer.sendRegistrationConfirmation({
                email: preferences.email,
                uuid: registration.uuid
            });

            res.status(201).end();
        } catch (err) {
            logger.error(err);
            res.status(500).end();
            await mailer.sendAdminNotification(err);
        }
    }

    static _validatePreferences(data) {
        if (!data.email || (/[^@]+@[^\.]+\..+/gi).test(data.email) === false) {
            return `Le courriel n'est pas valide.`;
        } else if (data.preferredStart && (/[0-9]{1,2}:[0-9]{2}/g).test(data.preferredStart) === false) {
            return 'Heure non valide. Doit être dans le format hh:mm (ex: 14:00).';
        } else if (data.preferredEnd && (/[0-9]{1,2}:[0-9]{2}/g).test(data.preferredEnd) === false) {
            return 'Heure non valide. Doit être dans le format hh:mm (ex: 14:00).';
        } else if (data.dailyScheduleAlert && (/[0-9]{1,2}:[0-9]{2}/g).test(data.dailyScheduleAlert) === false) {
            return 'Heure non valide. Doit être dans le format hh:mm (ex: 14:00).';
        } else if (data.preferredStart && !data.alertThreshold) {
            return `Veuillez spécifier un seuil de place disponible.`;
        } else if (data.alertThreshold && typeof data.alertThreshold !== 'number') {
            return `Le seuil de place disponible doit être un nombre.`;
        }
    }
}

module.exports = RegistrationController;