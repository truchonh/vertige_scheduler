const express = require('express');
const mongoose = require('mongoose');

const logger = require('../module/simpleLogger');
const mailer = require('./mailer');

const Preference = mongoose.model('preference', require('../collection/preference'));

class PreferenceController {
    static getRouter() {
        const router = express.Router();

        // router.route('/:preference_id').delete((req, res) => this._handleUnsubscribe(req, res));

        return router;
    }

    static async _handleUnsubscribe(req, res) {
        const preferenceId = req.params.preference_id;
        if (!preferenceId) {
            return res.status(404).end();
        }

        try {
            const preference = await Preference.findById(preferenceId);
            if (!preference) {
                return res.status(404).end();
            }

            await preference.remove();

            res.status(204).end();
        } catch (err) {
            logger.error(err);
            res.status(500).end();
            await mailer.sendAdminNotification(err);
        }
    }
}

module.exports = PreferenceController;
