const superagent = require(`superagent`);
const cheerio = require('cheerio');
const dayjs = require('dayjs');
const cron = require('cron');
const puppeteer = require('puppeteer');
const mongoose = require('mongoose');

const logger = require('../module/simpleLogger');
const mailer = require('./mailer');
const CalendarPeriod = require('../entity/CalendarPeriod');

const Schedule = mongoose.model('schedule', require('../collection/schedule'));
const Preference = mongoose.model('preference', require('../collection/preference'));
const CapacityPlotDataCache = mongoose.model('capacityPlotDataCache', require('../collection/capacityPlotDataCache'));

const VERTIGE_PRODUCT_ID = '22702';
const OVOLT_PRODUCT_ID = '33021';



class Crawler {
    static async start() {
        const scheduleUpdateWrapper = async () => {
            try {
                await this._periodicScheduleUpdateJob();
                await this._scrapeOVoltSchedule();
            } catch (err) {
                if (err.message.includes('ETIMEDOUT')) {
                    // do nothing, the website is most likely down
                    return;
                }
                logger.error(err);
                await mailer.sendAdminNotification(err);
            }
        };
        const checkAvailabilityJob = new cron.CronJob('*/15 8-21 * * *', scheduleUpdateWrapper);
        checkAvailabilityJob.start();
        const nightlyCheckAvailabilityJob = new cron.CronJob('0 22-23,0-7 * * *', scheduleUpdateWrapper);
        nightlyCheckAvailabilityJob.start();
        logger.log('Crawler job started successfully');

        // launch the crawl script right away asynchronously
        this._periodicScheduleUpdateJob().catch(err => {});
        this._scrapeOVoltSchedule().catch(err => {});
    }

    static async _scrapeOVoltSchedule() {
        try {
            let results = await this._scrapeAllSchedules(
                'https://ecom.o-possum.com/ovoltsherbrooke/wp-admin/admin-ajax.php', OVOLT_PRODUCT_ID
            );
            logger.debug(`Crawler found ${results.length} available periods @ O-Volt.`);
            const schedule = new Schedule({
                periods: results,
                source: 'ovolt'
            });
            await schedule.save();
        } catch (err) {
            logger.warning(err)
        }
    }

    static async _periodicScheduleUpdateJob() {
        let results = await this._scrapeAllSchedules(
            'https://ecom.o-possum.com/vertige-escalade/wp-admin/admin-ajax.php', VERTIGE_PRODUCT_ID
        );
        logger.debug(`Crawler found ${results.length} available periods @ Vertige.`);
        const schedule = new Schedule({
            periods: results,
            source: 'vertige'
        });
        await schedule.save();

        const availablePeriods = schedule.findDailyAvailableSlots().filter(_slot => _slot.capacity > 0);

        for (let userPreference of await Preference.find()) {
            const dailyNotificationSent = userPreference.lastNotificationTimestamp &&
                dayjs(userPreference.lastNotificationTimestamp).isSame(dayjs(), 'date');
            if (dailyNotificationSent) {
                continue;
            }

            const preferredSlot = userPreference.preferredStart && availablePeriods.find(_slot => {
                return _slot.startPeriod.rawTimeStart === userPreference.preferredStart;
            });

            if (preferredSlot && preferredSlot.capacity <= userPreference.alertThreshold) {

                await mailer.notifyPreferredSlotLowCapacity({
                    preferences: userPreference,
                    capacityLeft: preferredSlot.capacity,
                    availableSlots: availablePeriods,
                });
                userPreference.lastNotificationTimestamp = new Date();
                await userPreference.save();

            } else if (userPreference.dailyScheduleAlert && availablePeriods.length) {

                const dailyAlertTime = dayjs()
                    .hour(userPreference.dailyScheduleAlert.split(':')[0])
                    .minute(userPreference.dailyScheduleAlert.split(':')[1])
                    .second(0)
                    .millisecond(0);

                if (dailyAlertTime.isBefore(dayjs())) {
                    await mailer.sendDailyAvailableSlots({
                        preferences: userPreference,
                        availableSlots: availablePeriods,
                    });
                    userPreference.lastNotificationTimestamp = new Date();
                    await userPreference.save();
                }

            }
        }

        // update the cache plot data
        for (let dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
            const plotData = await Schedule.aggregateCapacityOverTime('vertige', dayOfWeek);
            const cacheEntry = await CapacityPlotDataCache.findOne({ dayOfWeek });
            if (cacheEntry) {
                cacheEntry.data = plotData;
                await cacheEntry.save();
            } else {
                const newEntry = new CapacityPlotDataCache({ dayOfWeek, data: plotData });
                await newEntry.save();
            }
        }
    }

    /**
     * Fetch the current schedule slots for the next 7 days.
     * @param {string} url
     * @param {string} productId
     * @returns {Promise<CalendarPeriod[]>}
     * @private
     */
    static async _scrapeAllSchedules(url, productId) {
        const aWeekFromNow = dayjs().add(7, 'day');
        const periods = [];

        for (let date = dayjs(); date.isBefore(aWeekFromNow); date = date.add(1, 'day')) {
            const dailyPeriods = await this._fetchSchedule(url, productId, date)
            periods.push(...dailyPeriods);
        }

        return periods;
    }

    /**
     * @param {string} url
     * @param {string} productId
     * @param {dayjs} date
     * @returns {Promise<CalendarPeriod[]>}
     * @private
     */
    static async _fetchSchedule(url, productId, date) {
        const res = await superagent.post(url)
            .field('action', 'phive_get_booked_datas_of_date')
            .field('product_id', productId)
            .field('date', date.format('YYYY-MM-DD'))
            .field('type', 'time-picker');

        // Throttle the query rate to reduce the load on this poor WordPress server
        await new Promise(resolve => setTimeout(resolve, 5000));

        const $ = cheerio.load(res.text.replace(/<br>/gi, ''));
        return $('.ph-calendar-date:not(.booking-disabled)').map((i, el) => {
            return new CalendarPeriod({
                time: $('.ph_calendar_time', el).text().toString(),
                capacity: $(el).hasClass('booking-full') ? '0' : $(el).attr('data-max').toString(),
                date: date
            });
        }).get();
    }

    /**
     * Check the availability of a specific calendar period.
     * @param {string} dateStr ISODate format
     * @param {string} timeStart
     * @returns {Promise<boolean>}
     */
    static async checkScheduleAvailability(dateStr, timeStart) {
        const date = dayjs(dateStr);

        const res = await superagent.post('https://ecom.o-possum.com/vertige-escalade/wp-admin/admin-ajax.php')
            .field('action', 'phive_get_booked_datas_of_date')
            .field('product_id', VERTIGE_PRODUCT_ID)
            .field('date', date.format('YYYY-MM-DD'))
            .field('type', 'time-picker');

        const $ = cheerio.load(res.text.replace(/<br>/gi, ''));
        const dailyPeriods = $('.ph-calendar-date:not(.booking-disabled):not(.booking-full)').map((i, el) => {
            return new CalendarPeriod({
                time: $('.ph_calendar_time', el).text().toString(),
                capacity: $('.ph_bookings_capacity', el).text().toString(),
                date: date
            });
        }).get();

        const schedule = new Schedule({
            periods: dailyPeriods
        });
        return schedule.findAvailableSlotsByDay(dateStr).some(_slot => _slot.startPeriod.rawTimeStart === timeStart);
    }

    /**
     * Order a booking on behalf of the user.
     * @param {object} formData
     * @returns {Promise<void>}
     */
    static async submitBooking(formData) {
        const browser = await puppeteer.launch({
            slowMo: 100,
            defaultViewport: { width: 1000, height: 800 },
            args: ['--no-sandbox', '--single-process', '--no-zygote'],
            // headless: false
        });
        const page = await browser.newPage();

        await page.goto('https://ecom.o-possum.com/vertige-escalade/produit/reservation-escalade-2-hrs/');
        await page.waitForSelector('#ph-calendar-days > .ph-calendar-date');

        // if the booked date is in a different month, change the calendar page
        if (dayjs(formData.bookingDate).month() > dayjs().month()) {
            const changeMonthSuccess = await page.evaluate(() => {
                const nextMonthBtn = document.querySelector('.time-calendar-date-section .ph-next');
                nextMonthBtn && nextMonthBtn.click();
                return !!nextMonthBtn;
            });
            if (!changeMonthSuccess) {
                await this._cleanupCrawler(browser);
                throw new Error('Failed to select the calendar month.');
            }
        }

        // wait for #ph-calendar-overlay to be invisible
        await new Promise(resolve => setTimeout(resolve, 1000));
        await page.waitForFunction(() => {
            const calendarOverlayEl = document.querySelector('#ph-calendar-overlay');
            return calendarOverlayEl && calendarOverlayEl.style.display === 'none';
        });

        // select the current date
        const selectDaySuccess = await page.evaluate(_date => {
            const calendarDays = Array.from(document.querySelectorAll(`#ph-calendar-days > .ph-calendar-date`));
            const selectedDay = calendarDays.find(_el => _el.querySelector(`input[value="${_date}"]`));
            selectedDay && selectedDay.click();
            return !!selectedDay;
        }, dayjs(formData.bookingDate).format('YYYY-MM-DD'));
        if (!selectDaySuccess) {
            await this._cleanupCrawler(browser);
            throw new Error('Failed to select the calendar day.');
        }
        await page.waitForSelector('#ph-calendar-time > .ph-calendar-date');

        // select the period
        const selectTimeSuccess = await page.evaluate(_selectedTime => {
            const calendarDays = Array.from(document.querySelectorAll(`#ph-calendar-time > .ph-calendar-date`));
            const selectedTime = calendarDays.find(_el => _el.querySelector(`input[value="${_selectedTime}"]`));
            selectedTime && selectedTime.click();
            return !!selectedTime;
        }, `${dayjs(formData.bookingDate).format('YYYY-MM-DD')} ${formData.timeStart}`);
        if (!selectTimeSuccess) {
            await this._cleanupCrawler(browser);
            throw new Error('Failed to select the time period.');
        }

        // wait for the "reserver" button to become enabled
        await page.waitForSelector('.ph_book_now_button:not(.disabled)');

        const addToCartSuccess = await page.evaluate(() => {
            const reserveBtn = document.querySelector('.ph_book_now_button:not(.disabled)');
            reserveBtn && reserveBtn.click();
            return !!reserveBtn;
        });
        if (!addToCartSuccess) {
            await this._cleanupCrawler(browser);
            throw new Error('Failed to add the booking period to the cart.');
        }

        // wait for the booking to complete
        await page.waitForNavigation({ waitUntil: ['load', 'domcontentloaded'] })

        await page.goto('https://ecom.o-possum.com/vertige-escalade/validation/');

        // fill the booking form
        await page.waitForSelector('#billing_first_name');

        await page.focus('#billing_first_name');
        await page.type('#billing_first_name', formData.name);
        await page.focus('#billing_last_name');
        await page.type('#billing_last_name', formData.family_name);
        // TODO: Add support for the country selection list (Canada is selected by default)
        await page.focus('#billing_address_1');
        await page.type('#billing_address_1', formData.street_address);
        await page.focus('#billing_city');
        await page.type('#billing_city', formData.city);
        // TODO: Add support for the province selection list (Quebec is selected by default)
        await page.focus('#billing_postcode');
        await page.type('#billing_postcode', formData.postal_code);
        await page.focus('#billing_phone');
        await page.type('#billing_phone', formData.phone);
        await page.focus('#billing_email');
        await page.type('#billing_email', formData.email);

        await page.evaluate(() => {
            document.querySelector('#terms').click()
        });

        await page.evaluate(() => {
            document.querySelector('#place_order').click()
        });

        await page.waitForNavigation({ waitUntil: ['load', 'domcontentloaded'] });
        if (!page.url().includes('/validation/order-received')) {
            await this._cleanupCrawler(browser);
            throw new Error('Failed to submit the order. Maybe a form validation failed ?');
        }

        await this._cleanupCrawler(browser);
    }

    static async _cleanupCrawler(browser) {
        await browser.close();
    }
}

module.exports = Crawler;
