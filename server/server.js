const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const graylog2 = require('graylog2');
const https = require('https')
const fs = require('fs');

require('dotenv').config();

const conn = require('./collection/database').db.connection;
const crawler = require('./controller/crawler');
const logger = require('./module/simpleLogger');

class Server {
    static async start() {
        await this._connectToDb();
        await crawler.start();

        const app = express();

        app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
        app.use(bodyParser.json({ limit: '5mb' }));

        app.use(express.static(path.join(__dirname, '..', 'client', 'build')));

        if (process.env.NODE_ENV === 'PROD') {
            const graylogLogger = new graylog2.graylog({
                servers: [
                    { host: process.env.GRAYLOG_HOST, port: process.env.GRAYLOG_PORT }
                ],
                hostname: 'api.finances-app.com',
                facility: 'Node.js'
            });
            graylogLogger.on('error', function (err) {
                logger.error('[Expressjs logger] Error while trying to write to graylog2:', err);
            });
            app.use(morgan('combined', {
                stream: { write: (message) => graylogLogger.log(message, { type: 'vertige-scheduler-express' }) }
            }));
        }

        app.use('/api/registration', require('./controller/registration').getRouter());
        app.use('/api/preference', require('./controller/preference').getRouter());
        app.use('/api/booking', require('./controller/booking').getRouter());
        app.use('/api/schedule', require('./controller/schedule').getRouter());

        app.get('/*', (req, res) => {
            res.sendFile(path.join(__dirname, '..', 'client', 'build', 'index.html'));
        });

        try {
            let credentials = {
                key: fs.readFileSync(process.env.HTTPS_PRIVK_PATH, 'utf8'),
                cert: fs.readFileSync(process.env.HTTPS_CERT_PATH, 'utf8')
            };
            let httpsServer = https.createServer(credentials, app);
            await httpsServer.listen(process.env.HTTPS_PORT);
            logger.log(`https server started on port ${process.env.HTTPS_PORT}`);
        } catch (err) {
            logger.error('Could not load the https credentials.');

            await app.listen(process.env.PORT);
        }
    }

    static async _connectToDb() {
        return new Promise((resolve, reject) => {
            logger.log(`Connecting to database..`);
            conn.on('error', err => reject(err));
            conn.once('open', () => resolve());
        });
    }
}

Server.start()
    .catch(err => {
        logger.critical(err);
        process.exit(1);
    });
