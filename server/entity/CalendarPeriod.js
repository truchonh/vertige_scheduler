class CalendarPeriod {
    /**
     * @param {string} timeStr
     * @param {string} capacityStr
     * @param {dayjs.Dayjs} date
     */
    constructor({ time: timeStr, capacity: capacityStr, date }) {
        const [ timeStart, timeEnd ] = timeStr.split(' - ');
        this.rawTimeStart = timeStart;
        this.dateStart = date.clone()
            .hour(parseInt(timeStart.substring(0, 2)))
            .minute(parseInt(timeStart.substring(3, 5)))
            .second(0)
            .millisecond(0);
        this.rawTimeEnd = timeEnd;
        this.dateEnd = date.clone()
            .hour(parseInt(timeEnd.substring(0, 2)))
            .minute(parseInt(timeEnd.substring(3, 5)))
            .second(0)
            .millisecond(0);
        this.capacity = parseInt(capacityStr || '0');
    }
}

module.exports = CalendarPeriod;
