class BookingSlot {
    /**
     * @returns {CalendarPeriod}
     */
    get startPeriod() {
        return this._startPeriod;
    }

    /**
     * @returns {CalendarPeriod}
     */
    get endPeriod() {
        return this._endPeriod;
    }

    /**
     * @returns {number}
     */
    get capacity() {
        return this._capacity;
    }

    /**
     * {CalendarPeriod[]} calendarPeriods
     */
    constructor({ calendarPeriods: periods }) {
        let isSequential = true;
        for (let j = 0; j < periods.length - 1; j++) {
            isSequential = isSequential && periods[j].rawTimeEnd === periods[j + 1].rawTimeStart;
        }
        if (!isSequential) {
            throw new Error('Calendar periods are not sequential.');
        }

        this._capacity = Math.min(...periods.map(_e => _e.capacity))
        this._startPeriod = periods.shift();
        this._endPeriod = periods.pop();
    }

    toHtml() {
        return `<li>${this.toString()}</li>`
    }

    toString() {
        return `(${this.capacity} dispo) ${this.startPeriod.rawTimeStart} - ${this.endPeriod.rawTimeEnd}`
    }

    toJSON() {
        return {
            capacity: this.capacity,
            startPeriod: this.startPeriod,
            endPeriod: this.endPeriod
        }
    }
}

module.exports = BookingSlot;
