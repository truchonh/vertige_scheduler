const mongoose = require('mongoose');
const logger = require('../module/simpleLogger');

mongoose.Promise = global.Promise;
mongoose
    .connect(process.env.MONGO_HOST + process.env.MONGO_DATABASE, {
        auth: {
            user: process.env.MONGO_USER,
            password: process.env.MONGO_PWD,
            authSource: 'admin',
        },
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .catch(err => logger.error('Error loading database: ' + err));

module.exports = {
    db: mongoose,
};
