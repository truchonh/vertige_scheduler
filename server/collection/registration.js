const database = require('./database');
const mongoose = database.db;
const Schema = mongoose.Schema;

/**
 * @class Registration
 * @mixes {Document}
 */
let registrationSchema = new Schema({
    creationDate: { type: Schema.Types.Date, default: Date.now },
    uuid: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    data: Schema.Types.Mixed
});

module.exports = registrationSchema;
