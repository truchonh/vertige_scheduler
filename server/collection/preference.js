const database = require('./database');
const mongoose = database.db;
const Schema = mongoose.Schema;

/**
 * @class Preference
 * @mixes {Document}
 */
let preferenceSchema = new Schema({
    email: { type: String, required: true, unique: true },
    preferredStart: String,
    preferredEnd: String,
    alertThreshold: Number,
    dailyScheduleAlert: String,
    lastNotificationTimestamp: Schema.Types.Date,
    lastBookingTimestamp: Schema.Types.Date,
});

module.exports = preferenceSchema;
