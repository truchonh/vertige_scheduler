const database = require('./database');
const mongoose = database.db;
const Schema = mongoose.Schema;

const capacityPlotDataCacheSchema = new Schema({
    dayOfWeek: { type: Number, required: true },
    data: [Schema.Types.Mixed]
});

module.exports = capacityPlotDataCacheSchema;