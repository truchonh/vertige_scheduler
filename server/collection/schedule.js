const database = require('./database');
const mongoose = database.db;
const Schema = mongoose.Schema;
const dayjs = require('dayjs');
const _ = require('lodash');

const BookingSlot = require('../entity/BookingSlot')

/**
 * @class Schedule
 * @mixes {Document}
 */
let scheduleSchema = new Schema({
    creationDate: { type: Schema.Types.Date, default: Date.now },
    periods: [{
        rawTimeStart: String,
        rawTimeEnd: String,
        dateStart: Schema.Types.Date,
        dateEnd: Schema.Types.Date,
        capacity: Number,
    }],
    source: String
});

/**
 * @returns {BookingSlot[]}
 */
scheduleSchema.methods.findDailyAvailableSlots = function() {
    return this.findAvailableSlotsByDay(dayjs().format('YYYY-MM-DD'));
};

/**
 * @param {string} dateStr
 * @returns {BookingSlot[]}
 */
scheduleSchema.methods.findAvailableSlotsByDay = function(dateStr) {
    const now = dayjs();
    const date = dayjs(dateStr, 'YYYY-MM-DD');

    const periods = this.periods.filter(_period => {
        const dateStart = dayjs(_period.dateStart);
        return dateStart.isAfter(now) && date.isSame(dateStart, 'date');
    });

    if (periods.length < 4) {
        return [];
    }

    return convertToBookingSlots(periods);
};

/**
 * @param {CalendarPeriod[]} periods
 * @returns {BookingSlot[]}
 */
function convertToBookingSlots(periods) {
    const availableSlots = [];
    for (let i = 0; i < periods.length - 3; i++) {
        const subSection = periods.slice(i, i + 4);

        let isSequential = true;
        for (let j = 0; j < subSection.length - 1; j++) {
            isSequential = isSequential && subSection[j].rawTimeEnd === subSection[j + 1].rawTimeStart;
        }

        if (isSequential) {
            availableSlots.push(new BookingSlot({ calendarPeriods: subSection }));
        }
    }
    return availableSlots;
}

/**
 * Aggregate scraping data from the past week by period and day of the week.
 * The data is plotted by the remaining capacity and the time difference between the scan date and the period start.
 *
 * This should give the user an idea of how fast bookings are scheduled and allow them to know how far they have to
 * book in advance.
 * @param {string} source
 * @param {number} dayOfWeek Day of week, from 0-6, Sunday to Saturday
 * @returns {Aggregate}
 */
scheduleSchema.statics.aggregateCapacityOverTime = async function(source, dayOfWeek) {
    const periods = await this.model('schedule').aggregate([{
        $match: {
            source,
            creationDate: {
                $gte: dayjs().subtract(7, 'day').toDate()
            }
        }
    }, {
        $sort: { creationDate: 1 }
    }, {
        $unwind: {
            path: '$periods'
        }
    }, {
        $addFields: {
            'periods.creationDate': '$creationDate'
        }
    }, {
        $replaceRoot: {
            newRoot: '$periods'
        }
    }]);

    return _.chain(convertToBookingSlots(periods))
        .map(_slot => ({
            ..._slot.toJSON(),
            distance: dayjs(_slot.startPeriod.dateStart).valueOf() - dayjs(_slot.startPeriod.creationDate).valueOf(),
            dayOfWeek: dayjs(_slot.startPeriod.dateStart).day()
        }))
        .filter(_slot => _slot.distance <= 1000*60*60*24*7 && _slot.dayOfWeek === dayOfWeek)
        .sortBy(_slot => -_slot.distance)
        .groupBy(_slot => _slot.startPeriod.rawTimeStart)
        .map((_slots, _period) => ({
            period: _period,
            plot: _slots.map(_slot => ({
                distance: _slot.distance,
                capacity: _slot.capacity
            }))
        }))
        .value();
}

module.exports = scheduleSchema;
